﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aozoraBot
{
    class tweetUtil
    {
        public tweetUtil()
        { }

        ~tweetUtil()
        { }

        private string apiKey;
        private string apiSecret;
        private string AccessToken;
        private string AccessTokenSecret;

        public void initialize()
        {
            this.apiKey            = Properties.Settings.Default.apikey;
            this.apiSecret         = Properties.Settings.Default.apiSecret;
            this.AccessToken       = Properties.Settings.Default.AccessToken;
            this.AccessTokenSecret = Properties.Settings.Default.AccessTokenSecret;
        }
        public bool postString(string poststring )
        {
            bool rtn = false;

            /* get access token */
            var tokens = CoreTweet.Tokens.Create(
            this.apiKey,            //"{API key}"
            this.apiSecret,         //"{API secret}"
            this.AccessToken,       //"{Access token}"
            this.AccessTokenSecret  //"{Access token secret}");
            );

            /* post  */
            var response = tokens.Statuses.Update(new { status = poststring });

            /* debug */
            Console.WriteLine(response.Entities);

            if (null != response)
            {
                rtn = true;
            }
            return rtn;
        }
    }
}
