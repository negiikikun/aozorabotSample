﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aozoraBot
{
    class myTrace
    {
        public myTrace()
        {
            this.curLevel = myTrace_level.MYT_TRACE;
        }

        ~myTrace()
        {}

       public enum myTrace_level{
	    MYT_UNDEF,
	    MYT_TRACE,
	    MYT_DEBUG,
	    MYT_INFO,
	    MYT_WARNING,
	    MYT_ERROR,
	    MYT_CRITICAL,
	    MYT_FATAL,
	    MYT_NOMSG,
        };

        public myTrace_level curLevel;

        public void setTraceLevel(myTrace_level level)
        {
            this.curLevel = level;
        }

        public void writeLine(myTrace_level level, string str)
        { 
            string levelStr="";

            /* check current trace level */
            if (level >= this.curLevel)
            {
                switch (level)
                {
                    case myTrace_level.MYT_UNDEF:
                        levelStr = "UNDEF";
                        break;
                    case myTrace_level.MYT_TRACE:
                        levelStr = "TRACE";
                        break;
                    case myTrace_level.MYT_DEBUG:
                        levelStr = "DEBUG";
                        break;
                    case myTrace_level.MYT_INFO:
                        levelStr = "INFO";
                        break;
                    case myTrace_level.MYT_WARNING:
                        levelStr = "WARNING";
                        break;
                    case myTrace_level.MYT_ERROR:
                        levelStr = "ERROR";
                        break;
                    case myTrace_level.MYT_CRITICAL:
                        levelStr = "CRITICAL";
                        break;
                    case myTrace_level.MYT_FATAL:
                        levelStr = "FATAL";
                        break;
                    case myTrace_level.MYT_NOMSG:
                        levelStr = "NOMSG";
                        break;
                    default:
                        break;
                }

                System.Diagnostics.StackFrame caller = new System.Diagnostics.StackFrame(1);
                string methodName = caller.GetMethod().Name;

                Console.WriteLine(methodName + ": [" + levelStr + "]" + str);
            }
        }
    }
}
