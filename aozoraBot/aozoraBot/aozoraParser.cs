﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Net;
using System.Collections;

namespace aozoraBot
{
    class aozoraParser : myHtmlParser
    {
        /* ctor */
        public aozoraParser()
        {
            this.postString = "";
            this.targetUrl = "";
            this.authorUrl = "";
            this.titleUrl = "";
            this.autor = "";
            this.title = "";
            this.spoilerText = "";
            this.ns = "http://www.w3.org/1999/xhtml";
            this.rand = new System.Random();
            this.maxpostlen = Properties.Settings.Default.maxlen;
            this.minpostlen = Properties.Settings.Default.minlen;
            this.maxretry = 5;
        }

        /* dtor */
        ~aozoraParser()
        { 
        }

        /* members */
        public  string postString;
        private string targetUrl;
        private string authorUrl;
        private string titleUrl;
        private string autor;
        private string title;
        private string spoilerText;
        private XNamespace ns;
        private System.Random rand;
        private uint maxpostlen;
        private uint minpostlen;
        private uint maxretry;

        /* methods */

        // <summary>URLがターゲットURLのフォーマットにマッチするか正規表現で判定</summary>
        // <param name="input">判定URL</param>
        // <returns>bool true:ターゲットURLとして適正</returns>
        private bool isTargetFormat(string input)
        {
            bool rtn = false;

            rtn = System.Text.RegularExpressions.Regex.IsMatch( input, 
                                                                @"/files/.*[.]html$", 
                                                                System.Text.RegularExpressions.RegexOptions.IgnoreCase
                                                                );
            return rtn;
        }

        // <summary>postに適切な文字列長、"「"、"」"のいずれかを含むような文字列でないか</summary>
        // <param name="input">判定文字列</param>
        // <returns>bool true:適正</returns>
        private bool isValidPostString(string input)
        {
            bool rtn = false;

            if (false == System.Text.RegularExpressions.Regex.IsMatch(input,
                                                                @"[「」]",
                                                                System.Text.RegularExpressions.RegexOptions.IgnoreCase
                                                                ))
            { 
                /* check length */
                if (input.Length > this.minpostlen)
                {   /* check twitter limit post length */
                    int sum = this.autor.Length + this.title.Length + input.Length;
                    if (sum < this.maxpostlen)
                    {
                        rtn = true;
                    }
                }
            }
            return rtn;
        }

        // <summary>inputを正規表現置換する</summary>
        // <param name="input">判定文字列</param>
        // <returns>string 置換結果</returns>
        private string replace(string input, string pattern, string format)
        {
            string rtn = "";

            rtn = System.Text.RegularExpressions.Regex.Replace( input,
                                                                pattern,
                                                                format
                                                                );
            return rtn;
        }

        // <summary>URLの最後の'/'以下をstripする.</summary>
        // <param name="input">対象URL</param>
        // <returns>string stripした新しい文字列</returns>
        private string getCurDirUrl(string input)
        {
            string result="";

            string[] split = input.Split('/');
            int count = 0;

            foreach (var itr in split)
            {
                if (count == (split.Count() - 1)) break;
                result += itr;
                result += "/";
                count++;
            }
            return result;
        }

        // <summary>著作者ページのURLをランダムに取得する.</summary>
        // <returns>bool true:成功 false:それ以外</returns>
        private bool getAuthorUrl()
        {
            bool rtn = false;

            /* get url at random */
            int idnum = this.rand.Next(1,1000);
            this.authorUrl = "http://www.aozora.gr.jp/index_pages/person" + idnum.ToString() +".html";
            rtn = true;
            return rtn;
        }

        // <summary>解析対象のURLを取得する. 
        //  1.著作者ページをランダムに選ぶ 
        //  2.著作者ページにリンクがある作品ページをまたランダムに選ぶ. 
        //  3.作品ページからHTML形式への作品リンクをターゲットURLとして採用 
        // </summary>
        // <returns>bool true:ターゲットURLを取得できた false:それ以外</returns>
        private bool getTargetUrl()
        {
            bool rtn = false;
            bool findFlg = false;
            XDocument autherDoc = null;

            while (false == findFlg)
            {
                /* set auther page url */
                this.getAuthorUrl();
                this.writeLine(myTrace_level.MYT_INFO, this.authorUrl);

                /* parse auther page. */
                autherDoc = this.getXDocument(this.authorUrl, Encoding.GetEncoding("UTF-8"));

                /* list links to titile pages. */
                var query = from n in autherDoc.Descendants("li").Elements("a").Where( g => g.Attribute("href").Value != null)
                            select n;

                this.writeLine(myTrace_level.MYT_INFO ,"query.Count()=" + query.Count());
                if (0 < query.Count())
                {
                    /* get random title */
                    int index = this.rand.Next(0, query.Count());
                    var element = query.ElementAt(index);

                    this.titleUrl = this.getCurDirUrl(this.authorUrl);
                    this.titleUrl += element.FirstAttribute.Value;

                    this.writeLine(myTrace_level.MYT_INFO,this.titleUrl);

                    /* get document from title page */
                    var titleDoc = this.getXDocument(this.titleUrl, Encoding.GetEncoding("UTF-8"));

                    if (null != titleDoc)
                    {
                        var query2 = from n in titleDoc.Descendants("tr").Elements("td").Elements("a").Where(g => g.Attribute("href").Value != null)
                                     select n;

                        this.writeLine(myTrace_level.MYT_INFO,"query2.Count()" + query2.Count());

                        if (0 < query2.Count())
                        {
                            foreach (var itr in query2)
                            {
                                if (true == this.isTargetFormat(itr.FirstAttribute.Value))
                                {
                                    this.targetUrl = this.getCurDirUrl(this.titleUrl);
                                    this.targetUrl += itr.FirstAttribute.Value;
                                    break;
                                }
                            }
                        }

                        if ( "" != this.targetUrl)
                        {
                            this.writeLine(myTrace_level.MYT_INFO,"this.targetUrl=" + this.targetUrl);
                            findFlg = true;
                        }
                    }  
                }

                /* sleep for iterae request */
                System.Threading.Thread.Sleep(1000);
            }

            if ("" != this.targetUrl)
            {
                rtn = true;
            }  
            return rtn;
        }

        // <summary>ターゲットURLから作品名、作者名、本文を取得する.</summary>
        // <param name="input">htmlをxmlにパースしたもの</param>
        // <returns>bool true:すべての値の取得に成功 false:それ以外</returns>
        private bool getMaintText(XDocument input)
        {
            bool rtn=false;

            /* get title in h1 header */
            var titleQuery =    from n in input.Descendants(this.ns + "h1").Where(g => g.Attribute("class").Value == "title")
                                select n;

            if (0 < titleQuery.Count())
            {
                this.title = titleQuery.ElementAt(0).Value;
            }
            else 
            {
                this.writeLine(myTrace_level.MYT_DEBUG, "titleQuery not found.");
            }

            /* get author in h2 header */
            var authorQuery = from n2 in input.Descendants(this.ns + "h2").Where(h => h.Attribute("class").Value == "author")
                              select n2;

            if (0 < authorQuery.Count())
            {
                this.autor = authorQuery.ElementAt(0).Value;
            }
            else
            {
                this.writeLine(myTrace_level.MYT_DEBUG, "authorQuery not found.");
            }

            /* get main text in div */
            var mainQuery = from n3 in input.Descendants(this.ns + "div").Where(i => i.Attribute("class").Value == "main_text")
                            select n3;

            if (0 < mainQuery.Count())
            {
                string mainTexts = mainQuery.ElementAt(0).Value;
                string[] splits = mainTexts.Split('。');
                int index = 0;
                var newArray = new ArrayList();

                /* create valid text array  */
                for (index = 0; index < splits.Count(); index++)
                {
                    /* remove new line code. */
                    string replaced = this.replace(splits.ElementAt(index), "\n", "");

                    if (true == this.isValidPostString(replaced))
                    {
                        /* regist */
                        newArray.Add(replaced);
                    }
                }

                if (0 < newArray.Count)
                {
                    index = this.rand.Next(0, newArray.Count);
                    this.spoilerText = newArray[index].ToString();
                    this.spoilerText += "。";
                }
            }
            else
            {
                this.writeLine(myTrace_level.MYT_DEBUG, "mainQuery not found.");
            }

            this.writeLine(myTrace_level.MYT_INFO, "this.title:"+this.title);
            this.writeLine(myTrace_level.MYT_INFO, "this.autor:" + this.autor);
            this.writeLine(myTrace_level.MYT_INFO, "this.spoilerText:" + this.spoilerText);

            if (("" != this.title) &&
                ("" != this.autor) &&
                ("" != this.spoilerText))
            {
                this.postString = this.spoilerText + " " + this.autor + "『" + this.title + "』";
                this.writeLine(myTrace_level.MYT_INFO, "this.postString:" + this.postString);
                rtn = true;
            }
            else 
            {
                this.writeLine(myTrace_level.MYT_DEBUG, "valid text not found.");
            }

            return rtn;
        }

        // <summary>urlをXMLでパースする.青空文庫はページによってエンコードが異なるため第二引数を追加した.</summary>
        // <param name="url">パース対象</param>
        // <param name="encode">対象URLのエンコード</param>
        // <returns>XDocument パース結果</returns>
        private XDocument getXDocument(string url, Encoding encode)
        {
            XDocument rtn=null;

            if (null != url)
            {
                /* get stream*/
                using (WebClient wc = new WebClient())
                {
                    bool    isRead = false;
                    int     reqCount = 0;
                    Stream  stream=null;

                    wc.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0)");
                    wc.UseDefaultCredentials = true;

                    while (false == isRead)
                    {
                        try
                        {
                            stream = wc.OpenRead(url);

                            using (StreamReader reader = new StreamReader(stream, encode))
                            {
                                /* parse */
                                rtn = this.parse(reader);
                                isRead = true;
                            }
                        }
                        catch (WebException e)
                        {
                            this.writeLine(myTrace_level.MYT_DEBUG, e.Message);
                            this.writeLine(myTrace_level.MYT_DEBUG, e.StackTrace);
                        }
                        finally 
                        {
                            if (null != stream)
                            {
                                stream.Close();
                                stream = null;
                            }
                        }

                        /* sometimes reqest url is acutually 404 not found. */
                        if (reqCount > this.maxretry)
                        {
                            break;
                        }

                        if (false == isRead)
                        {
                            reqCount++;
                            /* sleep for next request. */
                            System.Threading.Thread.Sleep(3000);
                        }
                    }
                }
            }
            return rtn;
        }

        // <summary>postするtweetをランダム生成する.</summary>
        // <returns>bool true:成功 false:それ以外</returns>
        public bool getPostString()
        {
            bool rtn = false;
            XDocument   mydoc=null;
            uint retryCount = 0;

            while (true)
            {
                /* get url at random */
                if (true == this.getTargetUrl())
                {
                    /* parse HTML to XML */
                    mydoc = this.getXDocument(this.targetUrl, Encoding.GetEncoding("Shift-JIS"));
                    if (null != mydoc)
                    {
                        /* get post string at ramdom */
                        if (true == this.getMaintText(mydoc))
                        {
                            rtn = true;
                            break;
                        }else{
                            this.writeLine(myTrace_level.MYT_WARNING, "getMaintText failed.");
                        }
                    }else{
                        this.writeLine(myTrace_level.MYT_WARNING, "mydoc is null.");
                    }
                }else{
                    this.writeLine(myTrace_level.MYT_WARNING, "getTargetUrl failed.");
                }

                if (retryCount > this.maxretry)
                {
                    break;
                }
                retryCount++;
            }
            return rtn;
        }
    }
}
