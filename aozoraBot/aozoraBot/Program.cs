﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;


namespace aozoraBot
{
    using AppFunc = Func<IDictionary<string, object>, Task>;

    class twitterBot
    {
        private static readonly ManualResetEvent _quitEvent = new ManualResetEvent(false);

        static void Main(string[] args)
        {
            /* set timer event */
            var myTimer = new System.Timers.Timer();

            myTimer.Elapsed += new ElapsedEventHandler(OnElapsed_TimersTimer);
            myTimer.Interval = 300000;

            /* start */
            myTimer.Start();

            /*wait*/
            Console.ReadLine();

            /* stop */
            myTimer.Stop();
        }

        static void OnElapsed_TimersTimer(object sender, ElapsedEventArgs e)
        {
            var myParser = new aozoraBot.aozoraParser();
            var myTweet = new aozoraBot.tweetUtil();

            /* get string to post */
            if (true == myParser.getPostString())
            {
                /* initiliaze: set apikeys.*/
                myTweet.initialize();
                /* post */
                myTweet.postString(myParser.postString);
            }
            else { 
                /* debug */
                Console.WriteLine("getPostString faield.\n");
            }
        }
    }
}
