﻿using System;
using System.Net;
using System.IO;
using System.Xml.Linq;
using Sgml;

namespace aozoraBot
{
    class myHtmlParser : myTrace
    {
         public myHtmlParser()
         {}

         ~myHtmlParser()
         {}

         public XDocument parse(TextReader inStream )
         {
             XDocument rtn = null;
             using ( SgmlReader myreader = new SgmlReader())
             {
                myreader.DocType = "HTML";
                myreader.CaseFolding = CaseFolding.ToLower;
                myreader.InputStream = inStream;
                myreader.IgnoreDtd   = true; //これ追加しないとAorozoraBnkoのHTML解析に失敗します。
                try
                {
                    rtn = XDocument.Load(myreader);
                }catch(WebException e){
                    
                    this.writeLine(myTrace_level.MYT_DEBUG, e.Message);
                    this.writeLine(myTrace_level.MYT_DEBUG, e.StackTrace);
                    System.Threading.Thread.Sleep(1000);
                }

                /* close */
                myreader.InputStream.Close();

                return rtn;
             }
         }
    }
    
}